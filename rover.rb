require_relative 'going_away_exception'
require_relative 'colliding_exception'

class Rover
  attr_reader :position, :has_completed_moves, :error

  def initialize(input, mars)
    @position = {
        :x => input.shift,
        :y => input.shift,
        :direction => input.shift
    }
    @movements = input.join.split(//)
    @mars = mars
    @error = false
  end

  def move
    initial_pos = @position
    begin
      @movements.each do |each_move|
        perform_move(each_move)
      end
    rescue GoingAwayException, CollidingException => e
      puts "Rover with initial position: #{initial_pos} has exception: #{e.message}"
      @error = true
    end
    @has_completed_moves = true
  end

  def to_s
    if !@error
      "#{@position[:x]} #{@position[:y]} #{@position[:direction]}"
    else
      "Error in moving Rover."
    end
  end

  private

  def perform_move(move)
    case move
      when 'L'
        turn_left
      when 'R'
        turn_right
      when 'M'
        move_forward
    end
  end

  def turn_left
    new_direction = nil
    case @position[:direction]
      when 'N'
        new_direction = 'W'
      when 'W'
        new_direction = 'S'
      when 'S'
        new_direction = 'E'
      when 'E'
        new_direction = 'N'
    end
    @position[:direction] = new_direction
  end

  def turn_right
    new_direction = nil
    case @position[:direction]
      when 'N'
        new_direction = 'E'
      when 'W'
        new_direction = 'N'
      when 'S'
        new_direction = 'W'
      when 'E'
        new_direction = 'S'
    end
    @position[:direction] = new_direction
  end

  def move_forward
    new_x = @position[:x]
    new_y = @position[:y]

    case @position[:direction]
      when 'N'
        new_y = new_y + 1
      when 'S'
        new_y = new_y - 1
      when 'E'
        new_x = new_x + 1
      when 'W'
        new_x = new_x - 1
    end
    raise CollidingException, "Rover colliding with another one" if @mars.is_colliding?(new_x, new_y)
    raise GoingAwayException, "Rover moving away from the designated plateau region" if !@mars.within_region?(new_x, new_y)
    @position[:x] = new_x
    @position[:y] = new_y
  end
end
