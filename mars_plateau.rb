require_relative 'rover'

class MarsPlateau

  def initialize(input_data)
    @rovers = []
    @input = input_data.split()
    mars_plateau_input
    rovers_input
  end

  def move_rovers
    @rovers.each(&:move)
  end

  def within_region?(x, y)
    x >= 0 && x <= @top_x && y >= 0 && y <= @top_y
  end

  def is_colliding?(x, y)
    @rovers.each do |rover|
      if rover.has_completed_moves && !rover.error
        return true if (rover.position[:x] == x && rover.position[:y] == y)
      end
    end
    false
  end

  def to_s
    @rovers.each(&:to_s).join("\n")
  end

  private

  def mars_plateau_input
    top_x = Integer(@input.shift)
    top_y = Integer(@input.shift)
    raise "Top x/y corrdinate of plateau is not positive integer UserInput:#{top_x}, #{top_y}" if (top_x <= 0 || top_y <= 0)
    @top_x = top_x
    @top_y = top_y
  end

  def rovers_input
    @input.each_slice(4) do |x, y, direction, movement|
      rover_x = Integer(x)
      rover_y = Integer(y)
      raise "Rover x/y coordinate is not postive integer:#{rover_x}, #{rover_y}" if (rover_x <= 0 || rover_y <= 0)
      @rovers << Rover.new([rover_x, rover_y, direction, movement], self)
    end
  end

end